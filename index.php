<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP SC Activity 3</title>
</head>
<body>
    <h2>Person</h3>
        <p><?= $person->printName(); ?></p>
    <h2>Developer</h3>
        <p><?= $developer->printName(); ?></p>
    <h2>Engineer</h3>
        <p><?= $engineer->printName(); ?></p>
</body>
</html>